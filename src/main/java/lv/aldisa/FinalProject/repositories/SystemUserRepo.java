package lv.aldisa.FinalProject.repositories;

import lv.aldisa.FinalProject.models.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemUserRepo extends JpaRepository<SystemUser,Long> {

    SystemUserRepo findByUsername(String username);
}
