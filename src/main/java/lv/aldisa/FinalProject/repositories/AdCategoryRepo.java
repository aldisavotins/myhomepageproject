package lv.aldisa.FinalProject.repositories;

import lv.aldisa.FinalProject.models.AdvertisementCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdCategoryRepo extends JpaRepository<AdvertisementCategory,Long> {
}
