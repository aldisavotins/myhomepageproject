package lv.aldisa.FinalProject.repositories;

import lv.aldisa.FinalProject.models.JobAdvertisement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobAdRepo extends JpaRepository<JobAdvertisement, Long> {
}
