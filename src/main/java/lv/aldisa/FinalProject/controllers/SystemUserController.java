package lv.aldisa.FinalProject.controllers;

import lv.aldisa.FinalProject.models.SystemUser;
import lv.aldisa.FinalProject.repositories.SystemUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SystemUserController {

    @Autowired
    SystemUserRepo systemUserRepo;

    @GetMapping("/register")
    public String getRegisterPage(Model model){
        model.addAttribute("newSystemUser", new SystemUser());
        return "register";
    }

    @PostMapping("/createNewUser")
    public String createNewUser(SystemUser systemUser, Model model){
        model.addAttribute("newSystemUser", systemUser);
        systemUserRepo.save(systemUser);
        return "registration_complete.html";
    }

}
