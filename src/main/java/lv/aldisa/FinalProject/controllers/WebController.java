package lv.aldisa.FinalProject.controllers;

import lv.aldisa.FinalProject.models.AdvertisementCategory;
import lv.aldisa.FinalProject.repositories.AdCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
public class WebController {

    @Autowired
    private AdCategoryRepo adCategoryRepo;

    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }

    @GetMapping("/main_view")
    public String getLoginView(Model model){
        model.addAttribute("newAdCategory",new AdvertisementCategory());
        List<AdvertisementCategory> advertisementCategoryList = adCategoryRepo.findAll();
        model.addAttribute("newAdCategoryList", advertisementCategoryList);
        return "main_view";
    }


}
