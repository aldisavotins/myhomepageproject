package lv.aldisa.FinalProject.controllers;

import lv.aldisa.FinalProject.models.AdvertisementCategory;
import lv.aldisa.FinalProject.models.JobAdvertisement;
import lv.aldisa.FinalProject.models.SystemUser;
import lv.aldisa.FinalProject.repositories.AdCategoryRepo;
import lv.aldisa.FinalProject.repositories.JobAdRepo;
import lv.aldisa.FinalProject.repositories.SystemUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class JobAdController {

    @Autowired
    private AdCategoryRepo adCategoryRepo;

    @Autowired
    private JobAdRepo jobAdRepo;

    @Autowired
    private SystemUserRepo systemUserRepo;


    @GetMapping("/ad_category_view")
    private String getAdvertisementCategoryJobAds(Model model){
        model.addAttribute("newJobAdvertisement", new JobAdvertisement());
        return "ad_category_view.html";
    }

    @PostMapping("/createNewJobAdvertisement")
    private String saveJobAdvertisement(AdvertisementCategory advertisementCategory,
                                        JobAdvertisement jobAdvertisement, Model model) {
        model.addAttribute("newJobAdvertisement", new JobAdvertisement());
        advertisementCategory = adCategoryRepo.getOne(1L);

        List<JobAdvertisement> jobAdvertisement1 = advertisementCategory.getJobAdvertisement();
        jobAdvertisement1.add(jobAdvertisement);
        advertisementCategory.setJobAdvertisement(jobAdvertisement1);
        jobAdRepo.save(jobAdvertisement);
        adCategoryRepo.save(advertisementCategory);

//        jobAdvertisement.setSystemUser(systemUserRepo.findAll());
        model.addAttribute("jobAdList");

        return "ad_category_view.html";
    }






}
