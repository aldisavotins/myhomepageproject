package lv.aldisa.FinalProject.controllers;

import lv.aldisa.FinalProject.models.AdvertisementCategory;
import lv.aldisa.FinalProject.models.JobAdvertisement;
import lv.aldisa.FinalProject.repositories.AdCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AdvertisementCategoryController {

    @Autowired
    private AdCategoryRepo adCategoryRepo;

    @PostMapping("/createNewAdCategory")
    private String saveAdvertisementCategory(AdvertisementCategory category, Model model) {
        model.addAttribute("newAdCategory", new AdvertisementCategory());
        adCategoryRepo.save(category);
        List<AdvertisementCategory> advertisementCategoryList = adCategoryRepo.findAll();
        model.addAttribute("newAdCategoryList", advertisementCategoryList);
        return "main_view.html";
    }


}
