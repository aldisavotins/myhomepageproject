package lv.aldisa.FinalProject.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "AdvertisementCategory")
public class AdvertisementCategory implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ADVERTISEMENT_CATEGORY_ID")
    private long advertisementCategoryId;
    @NotBlank
    private String categoryName;

    @OneToMany(cascade = CascadeType.ALL)
    private List<JobAdvertisement> jobAdvertisement = new ArrayList<>();

    public AdvertisementCategory() {

    }

    public long getId() {
        return advertisementCategoryId;
    }

    public void setId(long id) {
        this.advertisementCategoryId = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<JobAdvertisement> getJobAdvertisement() {
        return jobAdvertisement;
    }

    public void setJobAdvertisement(List<JobAdvertisement> jobAdvertisement) {
        this.jobAdvertisement = jobAdvertisement;
    }

}
