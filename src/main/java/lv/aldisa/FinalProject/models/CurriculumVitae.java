package lv.aldisa.FinalProject.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "curriculum_vitae")
public class CurriculumVitae implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    private SystemUser systemUser;

    private String name;
    private String surname;
    private String adress;
    private String email;
    private String birthDate;
    private String job1;
    private String job2;
    private String job3;
    private String education1;
    private String education2;
    private String education3;

    public CurriculumVitae() {
    }
}
