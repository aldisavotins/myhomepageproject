package lv.aldisa.FinalProject.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "job_advertisement")
public class JobAdvertisement implements Serializable {

    @Id
    @GeneratedValue
    private long advertisementId;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "system_user_ads",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "advertisement_id")
    )
    private List<SystemUser> systemUsers;

    private String positionName;
    private String adText;
    private String expirationDate;

    public JobAdvertisement() {
    }

    public long getId() {
        return advertisementId;
    }

    public void setId(long advertisementId) {
        this.advertisementId = advertisementId;
    }

    public List<SystemUser> getSystemUsers() {
        return systemUsers;
    }

    public void setSystemUser(List<SystemUser> systemUsers) {
        this.systemUsers = systemUsers;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getAdText() {
        return adText;
    }

    public void setAdText(String adText) {
        this.adText = adText;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}
