package lv.aldisa.FinalProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class FinalProjectApplication implements CommandLineRunner {

    public static void main(String[] args) {

	    SpringApplication.run(FinalProjectApplication.class, args);

	}
    @Override
    public void run(String... args) throws Exception {
        if (args.length > 0) {
            System.out.println(args[0]);
        }
    }

}
